<?php

use App\Http\Controllers\CategoriesController;
use App\Http\Controllers\FrontendController;
use App\Http\Controllers\TagsController;
use App\Http\Controllers\PostsController;
use App\Http\Controllers\UsersController;
use Illuminate\Support\Facades\Route;

Route::get('/', [FrontendController::class, 'index'])->name('home');
Route::get('/blogs/{post}', [FrontendController::class, 'show'])->name('blogs.show');
Route::get('/blogs/category/{category}', [FrontendController::class, 'category'])->name('blogs.category');
Route::get('/blogs/tags/{tag}', [FrontendController::class, 'tag'])->name('blogs.tag');

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';

//Application Routes
//NOTE : Have used raw URL instead of categories.destroy

Route::middleware(['auth'])->group(function () {
    Route::resource('categories', CategoriesController::class)->middleware(['auth']);
    Route::resource('tags', TagsController::class)->middleware(['auth']);
    Route::delete('posts/trash/{post}', [PostsController::class, 'trash'])->name('posts.trash');
    Route::get('/posts/trashed', [PostsController::class, 'trashed'])->name('posts.trashed');
    Route::put('posts/restore/{post}', [PostsController::class, 'restore'])->name('posts.restore');
    Route::resource('posts', PostsController::class);
});

Route::middleware(['auth', 'admin'])->group(function(){
    Route::get('/users', [UsersController::class, 'index'])->name('users.index');
    Route::put('/users/{user}/make-admin', [UsersController::class, 'makeAdmin'])->name('users.make-admin');
    Route::put('/users/{user}/revoke-admin', [UsersController::class, 'revokeAdmin'])->name('users.revoke-admin');
});

