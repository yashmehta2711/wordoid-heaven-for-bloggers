
yashm@DESKTOP-BJDMDRV MINGW64 ~/Documents/Degree/Backend/Framework/wordoid (feature/tags)
$ git status
On branch feature/tags
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
        modified:   resources/views/layouts/admin-panel/app.blade.php
        modified:   routes/web.php

Untracked files:
  (use "git add <file>..." to include in what will be committed)
        app/Http/Controllers/TagsController.php
        app/Http/Requests/Tags/
        resources/views/tags/

no changes added to commit (use "git add" and/or "git commit -a")

yashm@DESKTOP-BJDMDRV MINGW64 ~/Documents/Degree/Backend/Framework/wordoid (feature/tags)
$ git add .
warning: LF will be replaced by CRLF in resources/views/layouts/admin-panel/app.blade.php.
The file will have its original line endings in your working directory
warning: LF will be replaced by CRLF in routes/web.php.
The file will have its original line endings in your working directory
warning: LF will be replaced by CRLF in app/Http/Controllers/TagsController.php.
The file will have its original line endings in your working directory
warning: LF will be replaced by CRLF in app/Http/Requests/Tags/CreateTagRequest.php.
The file will have its original line endings in your working directory
warning: LF will be replaced by CRLF in app/Http/Requests/Tags/UpdateTagRequest.php.
The file will have its original line endings in your working directory
warning: LF will be replaced by CRLF in resources/views/tags/create.blade.php.
The file will have its original line endings in your working directory
warning: LF will be replaced by CRLF in resources/views/tags/edit.blade.php.
The file will have its original line endings in your working directory
warning: LF will be replaced by CRLF in resources/views/tags/index.blade.php.
The file will have its original line endings in your working directory

yashm@DESKTOP-BJDMDRV MINGW64 ~/Documents/Degree/Backend/Framework/wordoid (feature/tags)
$ git commit -m "performed CRUD operations on tags"
[feature/tags 425e64f] performed CRUD operations on tags
 8 files changed, 285 insertions(+)
 create mode 100644 app/Http/Controllers/TagsController.php
 create mode 100644 app/Http/Requests/Tags/CreateTagRequest.php
 create mode 100644 app/Http/Requests/Tags/UpdateTagRequest.php
 create mode 100644 resources/views/tags/create.blade.php
 create mode 100644 resources/views/tags/edit.blade.php
 create mode 100644 resources/views/tags/index.blade.php

yashm@DESKTOP-BJDMDRV MINGW64 ~/Documents/Degree/Backend/Framework/wordoid (feature/tags)
$ git status
On branch feature/tags
nothing to commit, working tree clean

yashm@DESKTOP-BJDMDRV MINGW64 ~/Documents/Degree/Backend/Framework/wordoid (feature/tags)
$ git merge feature/tags
Already up to date.

yashm@DESKTOP-BJDMDRV MINGW64 ~/Documents/Degree/Backend/Framework/wordoid (feature/tags)
$ git checkout master
Switched to branch 'master'

yashm@DESKTOP-BJDMDRV MINGW64 ~/Documents/Degree/Backend/Framework/wordoid (master)
$ git merge feature/tags
Updating 18c9f5d..425e64f
Fast-forward
 app/Http/Controllers/TagsController.php            | 99 ++++++++++++++++++++++
 app/Http/Requests/Tags/CreateTagRequest.php        | 30 +++++++
 app/Http/Requests/Tags/UpdateTagRequest.php        | 30 +++++++
 app/Models/Tag.php                                 | 13 +++
 .../2021_06_25_075837_create_tags_table.php        | 32 +++++++
 resources/views/layouts/admin-panel/app.blade.php  |  3 +
 resources/views/tags/create.blade.php              | 25 ++++++
 resources/views/tags/edit.blade.php                | 26 ++++++
 resources/views/tags/index.blade.php               | 70 +++++++++++++++
 routes/web.php                                     |  2 +
 10 files changed, 330 insertions(+)
 create mode 100644 app/Http/Controllers/TagsController.php
 create mode 100644 app/Http/Requests/Tags/CreateTagRequest.php
 create mode 100644 app/Http/Requests/Tags/UpdateTagRequest.php
 create mode 100644 app/Models/Tag.php
 create mode 100644 database/migrations/2021_06_25_075837_create_tags_table.php
 create mode 100644 resources/views/tags/create.blade.php
 create mode 100644 resources/views/tags/edit.blade.php
 create mode 100644 resources/views/tags/index.blade.php

yashm@DESKTOP-BJDMDRV MINGW64 ~/Documents/Degree/Backend/Framework/wordoid (master)
$ git branch -d feature/tags
Deleted branch feature/tags (was 425e64f).

yashm@DESKTOP-BJDMDRV MINGW64 ~/Documents/Degree/Backend/Framework/wordoid (master)
$ git branch
* master

yashm@DESKTOP-BJDMDRV MINGW64 ~/Documents/Degree/Backend/Framework/wordoid (master)
$ git push origin master
Enumerating objects: 46, done.
Counting objects: 100% (46/46), done.
Delta compression using up to 8 threads
Compressing objects: 100% (30/30), done.
Writing objects: 100% (31/31), 5.02 KiB | 1.00 MiB/s, done.
Total 31 (delta 15), reused 0 (delta 0), pack-reused 0
To https://gitlab.com/yashmehta2711/wordoid.git
   18c9f5d..425e64f  master -> master

yashm@DESKTOP-BJDMDRV MINGW64 ~/Documents/Degree/Backend/Framework/wordoid (master)
$
