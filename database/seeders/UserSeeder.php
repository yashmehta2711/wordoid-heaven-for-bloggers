<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Yash Shirish Mehta',
            'email' => 'yashmehta2711@gmail.com',
            'password' => Hash::make('abcd1234')
        ]);

        User::create([
            'name' => 'Prem Dinesh Mirani',
            'email' => 'premmirani9@gmail.com',
            'password' => Hash::make('abcd1234')
        ]);

        User::create([
            'name' => 'Smit Rajesh Vaghela',
            'email' => 'smitvaghela33@gmail.com',
            'password' => Hash::make('abcd1234')
        ]);

        User::create([
            'name' => 'Manav Ritesh Shah',
            'email' => 'manavshah28@gmail.com',
            'password' => Hash::make('abcd1234')
        ]);
    }
}
