<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Tag;
use App\Models\Post;
use Carbon\Carbon;
use Faker\Factory;
use Illuminate\Database\Seeder;

class BlogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categoryNews = Category::create(['name' => 'News']);
        $categoryDesign = Category::create(['name' => 'Design']);
        $categoryTechnology = Category::create(['name' => 'Technology']);
        $categoryEngineering = Category::create(['name' => 'Engineering']);
        $categoryEconomics = Category::create(['name' => 'Economics']);

        $tagCustomers = Tag::create(['name' => 'Customers']);
        $tagDesign = Tag::create(['name' => 'Design']);
        $tagLaravel = Tag::create(['name' => 'Laravel']);
        $tagCoding = Tag::create(['name' => 'Coding']);
        $tagFinance = Tag::create(['name' => 'Finance']);


        $post1 = Post::create([
            'title' => 'We relocated our office to HOME!',
            'excerpt' => Factory::create()->sentence(rand(10, 18)),
            'content' => Factory::create()->paragraphs(rand(3, 7), true),
            'image' => 'images/posts/1.jpg',
            'category_id' => $categoryDesign->id,
            'user_id' => 1,
            'published_at' => Carbon::now()->format('Y-m-d')
        ]);

        $post2 = Post::create([
            'title' => 'Lets make something creative!',
            'excerpt' => Factory::create()->sentence(rand(10, 18)),
            'content' => Factory::create()->paragraphs(rand(3, 7), true),
            'image' => 'images/posts/2.jpg',
            'category_id' => $categoryEngineering->id,
            'user_id' => 2,
            'published_at' => Carbon::now()->format('Y-m-d')
        ]);

        $post3 = Post::create([
            'title' => 'Corona Vaccination Drive is on!',
            'excerpt' => Factory::create()->sentence(rand(10, 18)),
            'content' => Factory::create()->paragraphs(rand(3, 7), true),
            'image' => 'images/posts/3.jpg',
            'category_id' => $categoryNews->id,
            'user_id' => 3,
            'published_at' => Carbon::now()->format('Y-m-d')
        ]);

        $post4 = Post::create([
            'title' => 'Why Engineering is coolest!',
            'excerpt' => Factory::create()->sentence(rand(10, 18)),
            'content' => Factory::create()->paragraphs(rand(3, 7), true),
            'image' => 'images/posts/4.jpg',
            'category_id' => $categoryEngineering->id,
            'user_id' => 4,
            'published_at' => Carbon::now()->format('Y-m-d')
        ]);

        $post5 = Post::create([
            'title' => 'Updates In Java!',
            'excerpt' => Factory::create()->sentence(rand(10, 18)),
            'content' => Factory::create()->paragraphs(rand(3, 7), true),
            'image' => 'images/posts/5.jpg',
            'category_id' => $categoryTechnology->id,
            'user_id' => 1,
            'published_at' => Carbon::now()->format('Y-m-d')
        ]);

        $post6 = Post::create([
            'title' => 'A ton of money!',
            'excerpt' => Factory::create()->sentence(rand(10, 18)),
            'content' => Factory::create()->paragraphs(rand(3, 7), true),
            'image' => 'images/posts/6.jpg',
            'category_id' => $categoryEconomics->id,
            'user_id' => 2,
            'published_at' => Carbon::now()->format('Y-m-d')
        ]);

        $post7 = Post::create([
            'title' => 'Updates in Laravel!',
            'excerpt' => Factory::create()->sentence(rand(10, 18)),
            'content' => Factory::create()->paragraphs(rand(3, 7), true),
            'image' => 'images/posts/7.jpg',
            'category_id' => $categoryTechnology->id,
            'user_id' => 3,
            'published_at' => Carbon::now()->format('Y-m-d')
        ]);

        $post8 = Post::create([
            'title' => 'Bull Market!',
            'excerpt' => Factory::create()->sentence(rand(10, 18)),
            'content' => Factory::create()->paragraphs(rand(3, 7), true),
            'image' => 'images/posts/8.jpg',
            'category_id' => $categoryEconomics->id,
            'user_id' => 4,
            'published_at' => Carbon::now()->format('Y-m-d')
        ]);

        $post9 = Post::create([
            'title' => 'Google Cloud!',
            'excerpt' => Factory::create()->sentence(rand(10, 18)),
            'content' => Factory::create()->paragraphs(rand(3, 7), true),
            'image' => 'images/posts/9.jpg',
            'category_id' => $categoryEngineering->id,
            'user_id' => 1,
            'published_at' => Carbon::now()->format('Y-m-d')
        ]);

        $post10 = Post::create([
            'title' => 'Black Economy!',
            'excerpt' => Factory::create()->sentence(rand(10, 18)),
            'content' => Factory::create()->paragraphs(rand(3, 7), true),
            'image' => 'images/posts/10.jpg',
            'category_id' => $categoryEconomics->id,
            'user_id' => 2,
            'published_at' => Carbon::now()->format('Y-m-d')
        ]);

        $post1->tags()->attach([$tagCustomers->id, $tagDesign->id]);
        $post2->tags()->attach([$tagDesign->id]);
        $post3->tags()->attach([$tagCustomers->id]);
        $post4->tags()->attach([$tagCoding->id]);
        $post5->tags()->attach([$tagCoding->id, $tagLaravel->id, $tagDesign->id]);
        $post6->tags()->attach([$tagFinance->id]);
        $post7->tags()->attach([$tagCoding->id, $tagLaravel->id]);
        $post8->tags()->attach([$tagFinance->id]);
        $post9->tags()->attach([$tagCoding->id]);
        $post10->tags()->attach([$tagFinance->id]);
    }
}
